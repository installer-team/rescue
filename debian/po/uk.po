# THIS FILE IS GENERATED AUTOMATICALLY FROM THE D-I PO MASTER FILES
# The master files can be found under packages/po/
#
# DO NOT MODIFY THIS FILE DIRECTLY: SUCH CHANGES WILL BE LOST
#
# translation of uk.po to Ukrainian
# translation of uk.po to
# Ukrainian messages for debian-installer.
# Copyright (C) 2003 Software in the Public Interest, Inc.
# This file is distributed under the same license as debian-installer.
#
#
# Translations from iso-codes:
# Eugeniy Meshcheryakov <eugen@debian.org>, 2005, 2006, 2007, 2010.
# Євгеній Мещеряков <eugen@debian.org>, 2008.
# Borys Yanovych <borys@yanovy.ch>, 2010, 2011.
# Maxim V. Dziumanenko <dziumanenko@gmail.com>, 2010.
# Yuri Chornoivan <yurchor@ukr.net>, 2010, 2011, 2012, 2013.
# Anton Gladky <gladk@debian.org>, 2011-2016
# Taras Panchenko <top39@ukr.net>, 2021.
#
msgid ""
msgstr ""
"Project-Id-Version: uk\n"
"Report-Msgid-Bugs-To: rescue@packages.debian.org\n"
"POT-Creation-Date: 2019-11-02 21:37+0100\n"
"PO-Revision-Date: 2021-12-03 23:44+0000\n"
"Last-Translator: Taras Panchenko <top39@ukr.net>\n"
"Language-Team: Ukrainian <translation-team-uk@lists.sourceforge.net>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n"
"%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"

#. Type: title
#. Description
#. Info message displayed when running in rescue mode
#. :sl2:
#: ../rescue-check.templates:2001
msgid "Rescue mode"
msgstr "Режим відновлення"

#. Type: text
#. Description
#. :sl1:
#: ../rescue-mode.templates:1001
msgid "Enter rescue mode"
msgstr "Ввійти до режиму відновлення системи"

#. Type: text
#. Description
#. :sl3:
#: ../rescue-mode.templates:2001
msgid "No partitions found"
msgstr "Розділи не знайдені"

#. Type: text
#. Description
#. :sl3:
#: ../rescue-mode.templates:2001
msgid ""
"The installer could not find any partitions, so you will not be able to "
"mount a root file system. This may be caused by the kernel failing to detect "
"your hard disk drive or failing to read the partition table, or the disk may "
"be unpartitioned. If you wish, you may investigate this from a shell in the "
"installer environment."
msgstr ""
"Встановлювач не зміг знайти жодного розділу, тому ви не зможете змонтувати "
"кореневу файлову систему. Це може статися через те, що ядро не змогло знайти "
"жорсткий диск чи не змогло прочитати таблицю розділів, також диск може бути "
"нерозбитий. Якщо бажаєте, то можете розслідувати це в оболонці встановлювача."

#. Type: select
#. Choices
#. :sl3:
#: ../rescue-mode.templates:3001
msgid "Assemble RAID array"
msgstr "Зібрати RAID-масив"

#. Type: select
#. Choices
#. :sl3:
#: ../rescue-mode.templates:3001
msgid "Do not use a root file system"
msgstr "Не використовувати кореневу файлову систему"

#. Type: select
#. Description
#. :sl3:
#: ../rescue-mode.templates:3002
msgid "Device to use as root file system:"
msgstr "Пристрій для використання в якості кореневої файлової системи:"

#. Type: select
#. Description
#. :sl3:
#: ../rescue-mode.templates:3002
msgid ""
"Enter a device you wish to use as your root file system. You will be able to "
"choose among various rescue operations to perform on this file system."
msgstr ""
"Введіть назву пристрою, який ви бажаєте використовувати як кореневу файлову "
"систему. Вам буде надано можливість вибрати серед різноманітних операцій які "
"можна провести з цією файловою системою."

#. Type: select
#. Description
#. :sl3:
#: ../rescue-mode.templates:3002
msgid ""
"If you choose not to use a root file system, you will be given a reduced "
"choice of operations that can be performed without one. This may be useful "
"if you need to correct a partitioning problem."
msgstr ""
"Якщо ви вирішите не використовувати кореневу файлову систему, то список "
"доступних для вас операцій буде скорочено, оскільки деякі з них стануть "
"недієздатними. Ця операція може бути корисною під час вирішення проблем з "
"розбиттям диска."

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:4001
msgid "No such device"
msgstr "Немає такого пристрою"

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:4001
msgid ""
"The device you entered for your root file system (${DEVICE}) does not exist. "
"Please try again."
msgstr ""
"Пристрій, який ви ввели для кореневої файлової системи, (${DEVICE}) не "
"існує. Спробуйте знову, будь ласка."

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:5001
msgid "Mount failed"
msgstr "Монтування не вдалося"

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:5001
msgid ""
"An error occurred while mounting the device you entered for your root file "
"system (${DEVICE}) on /target."
msgstr ""
"Виникла помилка під час монтування пристрою, який ви ввели для кореневої "
"файлової системи, (${DEVICE}) на /target."

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:5001
msgid "Please check the syslog for more information."
msgstr ""
"Щоб отримати більше інформації, перегляньте, будь ласка, системний журнал."

#. Type: select
#. Description
#. :sl3:
#: ../rescue-mode.templates:6001
msgid "Rescue operations"
msgstr "Рятувальні операції"

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:7001
msgid "Rescue operation failed"
msgstr "Рятувальна операція закінчилася невдачею"

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:7001
msgid "The rescue operation '${OPERATION}' failed with exit code ${CODE}."
msgstr ""
"Рятувальна операція „${OPERATION}“ закінчилася невдачею з кодом виходу "
"${CODE}."

#. Type: text
#. Description
#. :sl3:
#: ../rescue-mode.templates:8001
msgid "Execute a shell in ${DEVICE}"
msgstr "Запустити командну оболонку в ${DEVICE}"

#. Type: text
#. Description
#. :sl3:
#: ../rescue-mode.templates:9001
msgid "Execute a shell in the installer environment"
msgstr "Запустити командну оболонку в оточенні встановлювача"

#. Type: text
#. Description
#. :sl3:
#: ../rescue-mode.templates:10001
msgid "Choose a different root file system"
msgstr "Вибрати іншу кореневу файлову систему"

#. Type: text
#. Description
#. :sl3:
#: ../rescue-mode.templates:11001
msgid "Reboot the system"
msgstr "Перезавантажити систему"

#. Type: text
#. Description
#. :sl3:
#. Type: text
#. Description
#. :sl3:
#. Type: text
#. Description
#. :sl3:
#: ../rescue-mode.templates:12001 ../rescue-mode.templates:16001
#: ../rescue-mode.templates:17001
msgid "Executing a shell"
msgstr "Запуск командної оболонки"

#. Type: text
#. Description
#. :sl3:
#: ../rescue-mode.templates:12001
msgid ""
"After this message, you will be given a shell with ${DEVICE} mounted on \"/"
"\". If you need any other file systems (such as a separate \"/usr\"), you "
"will have to mount those yourself."
msgstr ""
"Після цього повідомлення вам буде надано оболонку з ${DEVICE} змонтованим на "
"„/“. Якщо вам потрібна будь-яка інша файлова система (наприклад, окрема "
"файлова система „/usr/“), ви повинні будете змонтувати її самостійно."

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:13001
msgid "Error running shell in /target"
msgstr "Помилка при запуску оболонки в /target"

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:13001
msgid ""
"A shell (${SHELL}) was found on your root file system (${DEVICE}), but an "
"error occurred while running it."
msgstr ""
"Оболонка (${SHELL}) була знайдена на вашій кореневій файловій системі "
"(${DEVICE}), однак при її запуску виникла помилка."

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:14001
msgid "No shell found in /target"
msgstr "Оболонка не знайдена у /target"

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:14001
msgid "No usable shell was found on your root file system (${DEVICE})."
msgstr ""
"На вашій кореневій файловій системі (${DEVICE}) не знайдена оболонка, яку "
"можна використати."

#. Type: text
#. Description
#. :sl3:
#: ../rescue-mode.templates:15001
msgid "Interactive shell on ${DEVICE}"
msgstr "Інтерактивна командна оболонка в ${DEVICE}"

#. Type: text
#. Description
#. :sl3:
#: ../rescue-mode.templates:16001
msgid ""
"After this message, you will be given a shell with ${DEVICE} mounted on \"/"
"target\". You may work on it using the tools available in the installer "
"environment. If you want to make it your root file system temporarily, run "
"\"chroot /target\". If you need any other file systems (such as a separate "
"\"/usr\"), you will have to mount those yourself."
msgstr ""
"Після цього повідомлення вам буде надано оболонку з ${DEVICE} змонтованим на "
"„/target“. Ви можете працювати з цією файловою системою використовуючи "
"інструменти наявні в оточенні встановлювача. Якщо ви бажаєте тимчасово "
"зробити її кореневою файловою системою, виконайте „chroot /target“. Якщо вам "
"потрібна будь-яка інша файлова система (наприклад, окрема файлова система „/"
"usr/“), ви повинні будете змонтувати її самостійно."

#. Type: text
#. Description
#. :sl3:
#: ../rescue-mode.templates:17001
msgid ""
"After this message, you will be given a shell in the installer environment. "
"No file systems have been mounted."
msgstr ""
"Після цього повідомлення вам буде надано оболонку в оточенні встановлювача. "
"Файлові системи монтуватися не будуть."

#. Type: text
#. Description
#. :sl3:
#: ../rescue-mode.templates:18001
msgid "Interactive shell in the installer environment"
msgstr "Інтерактивна командна оболонка в оточенні встановлювача"

#. Type: password
#. Description
#. :sl3:
#: ../rescue-mode.templates:19001
msgid "Passphrase for ${DEVICE}:"
msgstr "Пароль для ${DEVICE}:"

#. Type: password
#. Description
#. :sl3:
#: ../rescue-mode.templates:19001
msgid "Please enter the passphrase for the encrypted volume ${DEVICE}."
msgstr "Будь ласка, введіть парольну фразу для зашифрованого тому ${DEVICE}."

#. Type: password
#. Description
#. :sl3:
#: ../rescue-mode.templates:19001
msgid ""
"If you don't enter anything, the volume will not be available during rescue "
"operations."
msgstr ""
"Якщо ви нічого не введете, цей том не буде доступний під час виконання "
"відновлювальних операцій."

#. Type: multiselect
#. Choices
#. :sl3:
#: ../rescue-mode.templates:20001
msgid "Automatic"
msgstr "Автоматично"

#. Type: multiselect
#. Description
#. :sl3:
#: ../rescue-mode.templates:20002
msgid "Partitions to assemble:"
msgstr "Зібрати з таких розділів:"

#. Type: multiselect
#. Description
#. :sl3:
#: ../rescue-mode.templates:20002
msgid ""
"Select the partitions to assemble into a RAID array. If you select "
"\"Automatic\", then all devices containing RAID physical volumes will be "
"scanned and assembled."
msgstr ""
"Виберіть розділи для подальшої збірки в RAID-масив. Якщо ви виберете "
"\"Автоматично\", то буде проскановано та зібрано всі пристрої, що містять "
"фізичні томи RAID."

#. Type: multiselect
#. Description
#. :sl3:
#: ../rescue-mode.templates:20002
msgid ""
"Note that a RAID partition at the end of a disk may sometimes cause that "
"disk to be mistakenly detected as containing a RAID physical volume. In that "
"case, you should select the appropriate partitions individually."
msgstr ""
"Зверніть увагу, що розташування розділу RAID наприкінці диска час від часу "
"може призводити до помилкового визначення його як такого, що містить "
"фізичний том RAID. В цьому випадку слід вибрати придатні розділи самостійно."

#. Type: boolean
#. Description
#. :sl3:
#: ../rescue-mode.templates:21001
msgid "Mount separate ${FILESYSTEM} partition?"
msgstr "Змонтувати окремий розділ ${FILESYSTEM}?"

#. Type: boolean
#. Description
#. :sl3:
#: ../rescue-mode.templates:21001
msgid "The installed system appears to use a separate ${FILESYSTEM} partition."
msgstr ""
"Здається, у встановленій системі використовується окремих розділ "
"${FILESYSTEM}."

#. Type: boolean
#. Description
#. :sl3:
#: ../rescue-mode.templates:21001
msgid ""
"It is normally a good idea to mount it as it will allow operations such as "
"reinstalling the boot loader. However, if the file system on ${FILESYSTEM} "
"is corrupt then you may want to avoid mounting it."
msgstr ""
"Зазвичай, такий розділ варто змонтувати, оскільки за його допомогою можна "
"виконувати певні корисні дії, зокрема повторне встановлення завантажувача "
"системи. Втім, якщо файлову систему на розділі ${FILESYSTEM} пошкоджено, "
"ймовірно, варто уникати монтування такого розділу."
