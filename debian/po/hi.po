# THIS FILE IS GENERATED AUTOMATICALLY FROM THE D-I PO MASTER FILES
# The master files can be found under packages/po/
#
# DO NOT MODIFY THIS FILE DIRECTLY: SUCH CHANGES WILL BE LOST
#
# translation of debian-installer_packages_po_sublevel1 to Hindi
# Debian Installer master translation file template
# Don't forget to properly fill-in the header of PO files
# Debian Installer translators, please read the D-I i18n documentation
# in doc/i18n/i18n.txt
#
#
#
# Translations from iso-codes:
#   Data taken from ICU-2.8; originally from:
#   - Shehnaz Nagpurwala and Anwar Nagpurwala [first version]
#   - IBM NLTC:  http://w3.torolab.ibm.com/gcoc/documents/india/hi-nlsgg.htm
#   - Arundhati Bhowmick [IBM Cupertino]
#
#
# Nishant Sharma <me@nishants.net>, 2005, 2006.
# Kumar Appaiah <akumar@ee.iitm.ac.in>, 2008.
# Kumar Appaiah <a.kumar@alumni.iitm.ac.in>, 2008, 2009, 2010.
# Kumar Appaiah <kumar.appaiah@gmail.com>, 2009.
#   Alastair McKinstry <mckinstry@debian.org>, 2004.
# Kumar Appaiah <a.kumar@alumni.iitm.ac.in>, 2008.
# Kumar Appaiah <kumar.appaiah@gmail.com>, 2008, 2011, 2012.
# Sruthi Chandran <srud@openmailbox.org>, 2018.
# Himanshu Awasthi <johim9493@gmail.com>, 2018.
# KushagraKarira <kushagrakarira@gmail.com>, 2020, 2021.
# षिखर् <dnitesigm@gmail.com>, 2020.
# Indrani Roy <indrani@scriptek.co.in>, 2021.
#
msgid ""
msgstr ""
"Project-Id-Version: debian-installer_packages_po_sublevel1_hi\n"
"Report-Msgid-Bugs-To: rescue@packages.debian.org\n"
"POT-Creation-Date: 2019-11-02 21:37+0100\n"
"PO-Revision-Date: 2021-01-20 18:26+0000\n"
"Last-Translator: KushagraKarira <kushagrakarira@gmail.com>\n"
"Language-Team: \n"
"Language: hi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#. Type: title
#. Description
#. Info message displayed when running in rescue mode
#. :sl2:
#: ../rescue-check.templates:2001
msgid "Rescue mode"
msgstr "बचाव मोड"

#. Type: text
#. Description
#. :sl1:
#: ../rescue-mode.templates:1001
msgid "Enter rescue mode"
msgstr "बचाव मोड में प्रवेश करें"

#. Type: text
#. Description
#. :sl3:
#: ../rescue-mode.templates:2001
msgid "No partitions found"
msgstr "कोई पार्टीशन नहीं मिला"

#. Type: text
#. Description
#. :sl3:
#: ../rescue-mode.templates:2001
msgid ""
"The installer could not find any partitions, so you will not be able to "
"mount a root file system. This may be caused by the kernel failing to detect "
"your hard disk drive or failing to read the partition table, or the disk may "
"be unpartitioned. If you wish, you may investigate this from a shell in the "
"installer environment."
msgstr ""
"संस्थापक को कोई पार्टिशन न मिलने के कारण रूट फाइल सिस्टम माउंट नहीं किया जा सकता. आपके "
"ड्राइव को पहचानने में कर्नल की असफलता या डिस्क में पार्टिशन न होना इसके कुछ कारण हो सकते "
"हैं. आप चाहें तो इसे संस्थापक के एक शैल से इस समस्या के बारे में अधिक जानकारी प्राप्त कर सकते "
"हैं."

#. Type: select
#. Choices
#. :sl3:
#: ../rescue-mode.templates:3001
msgid "Assemble RAID array"
msgstr "रेड अरे एकाग्र करें"

#. Type: select
#. Choices
#. :sl3:
#: ../rescue-mode.templates:3001
msgid "Do not use a root file system"
msgstr "रूट फ़ाइल सिस्टम का उपयोग न करें"

#. Type: select
#. Description
#. :sl3:
#: ../rescue-mode.templates:3002
msgid "Device to use as root file system:"
msgstr "रूट फ़ाइल सिस्टम की तरह प्रयोग करने के लिए उपकरण:"

#. Type: select
#. Description
#. :sl3:
#: ../rescue-mode.templates:3002
msgid ""
"Enter a device you wish to use as your root file system. You will be able to "
"choose among various rescue operations to perform on this file system."
msgstr ""
"वह उपकरण बताएँ जिसे आप रूट फाइलतंत्र की तरह प्रयोग करना चाहते हैं. आप इस फाइलतंत्र पर "
"किये जाने वाले कई बचाव कार्यों को चुन सकेंगे."

#. Type: select
#. Description
#. :sl3:
#: ../rescue-mode.templates:3002
msgid ""
"If you choose not to use a root file system, you will be given a reduced "
"choice of operations that can be performed without one. This may be useful "
"if you need to correct a partitioning problem."
msgstr ""
"रूट फ़ैल तंत्र न रखने का अगर आप फैसला करें तो इसके बिना जोकार्य साध्य हैं, वह प्रस्तुत किये "
"जायेंगे. यह पार्टीशन त्रुटियों को ठीक करनेके लिए लाभदायक हो सकता है."

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:4001
msgid "No such device"
msgstr "ऐसा कोई उपकरण नहीं है"

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:4001
msgid ""
"The device you entered for your root file system (${DEVICE}) does not exist. "
"Please try again."
msgstr ""
"अपने रूट फाइलतंत्र के लिए जो उपकरण आपने प्रविष्ट किया है (${DEVICE}) वह उपस्थित नहीं है. "
"कृपया पुनः प्रयास करें."

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:5001
msgid "Mount failed"
msgstr "माउंट असफल"

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:5001
msgid ""
"An error occurred while mounting the device you entered for your root file "
"system (${DEVICE}) on /target."
msgstr ""
"आपके द्वारा रूट फाइलतंत्र के लिए चुने गये उपकरण (${DEVICE}) को /target पर माउंट करने में "
"त्रुटि हुई."

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:5001
msgid "Please check the syslog for more information."
msgstr "अधिक जानकारी के लिए कृपया सिसलॉग देखें."

#. Type: select
#. Description
#. :sl3:
#: ../rescue-mode.templates:6001
msgid "Rescue operations"
msgstr "राहत एवं बचाव कार्य"

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:7001
msgid "Rescue operation failed"
msgstr "बचाव प्रक्रिया असफल"

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:7001
msgid "The rescue operation '${OPERATION}' failed with exit code ${CODE}."
msgstr "बचाव प्रक्रिया '${OPERATION}' एग्ज़िट कोड ${CODE} के साथ असफल हुई."

#. Type: text
#. Description
#. :sl3:
#: ../rescue-mode.templates:8001
msgid "Execute a shell in ${DEVICE}"
msgstr "${DEVICE} में एक शेल चलाएँ"

#. Type: text
#. Description
#. :sl3:
#: ../rescue-mode.templates:9001
msgid "Execute a shell in the installer environment"
msgstr "संस्थापक परिवेश में एक शैल चलाएँ"

#. Type: text
#. Description
#. :sl3:
#: ../rescue-mode.templates:10001
msgid "Choose a different root file system"
msgstr "भिन्न रूट फ़ाइल सिस्टम चुनें"

#. Type: text
#. Description
#. :sl3:
#: ../rescue-mode.templates:11001
msgid "Reboot the system"
msgstr "तंत्र को रीबूट करें"

#. Type: text
#. Description
#. :sl3:
#. Type: text
#. Description
#. :sl3:
#. Type: text
#. Description
#. :sl3:
#: ../rescue-mode.templates:12001 ../rescue-mode.templates:16001
#: ../rescue-mode.templates:17001
msgid "Executing a shell"
msgstr "एक शेल चला रहे हैं"

#. Type: text
#. Description
#. :sl3:
#: ../rescue-mode.templates:12001
msgid ""
"After this message, you will be given a shell with ${DEVICE} mounted on \"/"
"\". If you need any other file systems (such as a separate \"/usr\"), you "
"will have to mount those yourself."
msgstr ""
"इस संदेश के बाद आपको \"/\" पर माउंट किये हुए ${DEVICE} के साथ एक शैल दिया जाएगा. यदि "
"आपको अन्य फाइलतंत्रों की आवश्यकता पड़ती है (जैसे कि एक अलग \"/usr\"), तो उसे आपको स्वयं "
"माउंट करना होगा."

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:13001
msgid "Error running shell in /target"
msgstr "/target में शैल चलाने में त्रुटि"

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:13001
msgid ""
"A shell (${SHELL}) was found on your root file system (${DEVICE}), but an "
"error occurred while running it."
msgstr ""
"आपके रूट फाइलतंत्र (${DEVICE}) पर एक शैल (${SHELL}) पाया गया, पर उसे चालू करते समय "
"एक त्रुटि हो गई."

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:14001
msgid "No shell found in /target"
msgstr "/target में कोई शैल नहीं पाया गया"

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:14001
msgid "No usable shell was found on your root file system (${DEVICE})."
msgstr "आपके रूट फाइलतंत्र (${DEVICE}) में कोई भी उपयोगी शैल नहीं पाया गया."

#. Type: text
#. Description
#. :sl3:
#: ../rescue-mode.templates:15001
msgid "Interactive shell on ${DEVICE}"
msgstr "${DEVICE} में इन्टरैक्टिव शेल"

#. Type: text
#. Description
#. :sl3:
#: ../rescue-mode.templates:16001
msgid ""
"After this message, you will be given a shell with ${DEVICE} mounted on \"/"
"target\". You may work on it using the tools available in the installer "
"environment. If you want to make it your root file system temporarily, run "
"\"chroot /target\". If you need any other file systems (such as a separate "
"\"/usr\"), you will have to mount those yourself."
msgstr ""
"इस संदेश के बाद ${DEVICE} को \"/target\" पर माउंट करके आपको एक शैल दिया जाएगा. आप "
"संस्थापक परिवेश में उपलब्ध औजारों का उपयोग करके उस शैल में कार्य कर सकते हैं. यदि आप इसे "
"अस्थायी रूप से अपना रूट फाइलतंत्र बनाना चाहते हैं तो \"chroot /target\" चलाएँ. यदि "
"आपको अन्य फाइलतंत्रों (जैसे कि एक पृथक \"/usr\" ) की आवश्यकता पड़ती है तो उन्हें आपको स्वयं "
"माउंट करना होगा."

#. Type: text
#. Description
#. :sl3:
#: ../rescue-mode.templates:17001
msgid ""
"After this message, you will be given a shell in the installer environment. "
"No file systems have been mounted."
msgstr ""
"इस संदेश के बाद आपको संस्थापक के अंदर एक शैल दिया जाएगा.कोई फाइल सिस्टम माउंट नहीं किया "
"गया."

#. Type: text
#. Description
#. :sl3:
#: ../rescue-mode.templates:18001
msgid "Interactive shell in the installer environment"
msgstr "संस्थापक परिवेश में इंटरैक्टिव शैल चलाएँ"

#. Type: password
#. Description
#. :sl3:
#: ../rescue-mode.templates:19001
msgid "Passphrase for ${DEVICE}:"
msgstr "${DEVICE} के लिए पासफ़्रेज़:"

#. Type: password
#. Description
#. :sl3:
#: ../rescue-mode.templates:19001
msgid "Please enter the passphrase for the encrypted volume ${DEVICE}."
msgstr "${DEVICE} को कूटबद्ध करने के लिए आपको एक कूटवाक्यांश लिखें."

#. Type: password
#. Description
#. :sl3:
#: ../rescue-mode.templates:19001
msgid ""
"If you don't enter anything, the volume will not be available during rescue "
"operations."
msgstr ""
"यदि आप कुछ भी दर्ज नहीं करते हैं, तो बचाव कार्यों के दौरान वॉल्यूम उपलब्ध नहीं होगा।"

#. Type: multiselect
#. Choices
#. :sl3:
#: ../rescue-mode.templates:20001
msgid "Automatic"
msgstr "स्वचालित"

#. Type: multiselect
#. Description
#. :sl3:
#: ../rescue-mode.templates:20002
msgid "Partitions to assemble:"
msgstr "किन पार्टीशन को एकाग्र करें:"

#. Type: multiselect
#. Description
#. :sl3:
#: ../rescue-mode.templates:20002
msgid ""
"Select the partitions to assemble into a RAID array. If you select "
"\"Automatic\", then all devices containing RAID physical volumes will be "
"scanned and assembled."
msgstr ""
"रेड अरे मैं एकाग्र करने के लिए पार्टीशन चुनें. अगर आप\"स्वचालित\" चुनेंगे, तो सारे उपकरण, "
"जिनमें रेड फिज़िकल वाल्यूम हों,उन्हें स्कैन करके एकाग्र किया जाएगा."

#. Type: multiselect
#. Description
#. :sl3:
#: ../rescue-mode.templates:20002
msgid ""
"Note that a RAID partition at the end of a disk may sometimes cause that "
"disk to be mistakenly detected as containing a RAID physical volume. In that "
"case, you should select the appropriate partitions individually."
msgstr ""
"ध्यान दें कि डिस्क के अंत में एक RAID विभाजन कभी-कभी उस डिस्क को गलती से पहचानने का "
"कारण हो सकता है जिसमें RAID भौतिक आयतन होता है। उस स्थिति में, आपको व्यक्तिगत रूप से "
"उपयुक्त विभाजन का चयन करना चाहिए।"

#. Type: boolean
#. Description
#. :sl3:
#: ../rescue-mode.templates:21001
msgid "Mount separate ${FILESYSTEM} partition?"
msgstr "माउंट ${FILESYSTEM} विभाजन?"

#. Type: boolean
#. Description
#. :sl3:
#: ../rescue-mode.templates:21001
msgid "The installed system appears to use a separate ${FILESYSTEM} partition."
msgstr "स्थापित सिस्टम एक अलग ${FILESYSTEM} विभाजन का उपयोग करता प्रतीत होता है।"

#. Type: boolean
#. Description
#. :sl3:
#: ../rescue-mode.templates:21001
msgid ""
"It is normally a good idea to mount it as it will allow operations such as "
"reinstalling the boot loader. However, if the file system on ${FILESYSTEM} "
"is corrupt then you may want to avoid mounting it."
msgstr ""
"यह आमतौर पर इसे माउंट करने के लिए एक अच्छा विचार है क्योंकि यह बूट लोडर को फिर से "
"स्थापित करने जैसे संचालन की अनुमति देगा। हालांकि, यदि ${FILESYSTEM} पर फ़ाइल सिस्टम "
"भ्रष्ट है, तो आप इसे माउंट करने से बचना चाहते हैं।"
