# THIS FILE IS GENERATED AUTOMATICALLY FROM THE D-I PO MASTER FILES
# The master files can be found under packages/po/
#
# DO NOT MODIFY THIS FILE DIRECTLY: SUCH CHANGES WILL BE LOST
#
# Turkish messages for debian-installer.
# Copyright (C) 2003, 2004 Software in the Public Interest, Inc.
# This file is distributed under the same license as debian-installer.
# Recai Oktaş <roktas@omu.edu.tr>, 2004, 2005, 2008.
# Osman Yüksel <yuxel@sonsuzdongu.com>, 2004.
# Özgür Murat Homurlu <ozgurmurat@gmx.net>, 2004.
# Halil Demirezen <halild@bilmuh.ege.edu.tr>, 2004.
# Murat Demirten <murat@debian.org>, 2004.
# Mert Dirik <mertdirik@gmail.com>, 2008-2012, 2014.
# Translations from iso-codes:
# Alastair McKinstry <mckinstry@computer.org>, 2001.
# (translations from drakfw)
# Fatih Demir <kabalak@gmx.net>, 2000.
# Free Software Foundation, Inc., 2000,2004
# Kemal Yilmaz <kyilmaz@uekae.tubitak.gov.tr>, 2001.
# Mert Dirik <mertdirik@gmail.com>, 2008, 2014.
# Nilgün Belma Bugüner <nilgun@fide.org>, 2001.
# Recai Oktaş <roktas@omu.edu.tr>, 2004.
# Tobias Quathamer <toddy@debian.org>, 2007.
# Translations taken from ICU SVN on 2007-09-09
# Ömer Fadıl USTA <omer_fad@hotmail.com>, 1999.
msgid ""
msgstr ""
"Project-Id-Version: debian-installer\n"
"Report-Msgid-Bugs-To: rescue@packages.debian.org\n"
"POT-Creation-Date: 2019-11-02 21:37+0100\n"
"PO-Revision-Date: 2015-06-30 01:49+0200\n"
"Last-Translator: Mert Dirik <mertdirik@gmail.com>\n"
"Language-Team: Debian L10N Turkish\n"
"Language: tr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. Type: title
#. Description
#. Info message displayed when running in rescue mode
#. :sl2:
#: ../rescue-check.templates:2001
msgid "Rescue mode"
msgstr "Kurtarma kipi"

#. Type: text
#. Description
#. :sl1:
#: ../rescue-mode.templates:1001
msgid "Enter rescue mode"
msgstr "Kurtarma kipine gir"

#. Type: text
#. Description
#. :sl3:
#: ../rescue-mode.templates:2001
msgid "No partitions found"
msgstr "Herhangi bir bölüm bulunamadı"

#. Type: text
#. Description
#. :sl3:
#: ../rescue-mode.templates:2001
msgid ""
"The installer could not find any partitions, so you will not be able to "
"mount a root file system. This may be caused by the kernel failing to detect "
"your hard disk drive or failing to read the partition table, or the disk may "
"be unpartitioned. If you wish, you may investigate this from a shell in the "
"installer environment."
msgstr ""
"Kurulum programı herhangi bir dosya sistemi bulamadı, bu nedenle kök dosya "
"sistemini bağlayamayacaksınız. Bu sorununa çekirdeğin sabit disk sürücünüzü "
"algılayamaması, bölüm tablosunu okuyamaması; ya da diskinizin bölümlenmemiş "
"olması neden olabilir. Dilerseniz bu sorunu kurulum ortamındaki bir kabuktan "
"inceleyebilirsiniz."

#. Type: select
#. Choices
#. :sl3:
#: ../rescue-mode.templates:3001
msgid "Assemble RAID array"
msgstr "RAID dizisi birleştir"

#. Type: select
#. Choices
#. :sl3:
#: ../rescue-mode.templates:3001
msgid "Do not use a root file system"
msgstr "Kök dosya sistemi kullanma"

#. Type: select
#. Description
#. :sl3:
#: ../rescue-mode.templates:3002
msgid "Device to use as root file system:"
msgstr "Kök dosya sistemi olarak kullanılacak aygıt:"

#. Type: select
#. Description
#. :sl3:
#: ../rescue-mode.templates:3002
msgid ""
"Enter a device you wish to use as your root file system. You will be able to "
"choose among various rescue operations to perform on this file system."
msgstr ""
"Kök dosya sistemi olarak kullanılmasını istediğiniz bir aygıt girin. Bu "
"dosya sisteminde daha sonra çeşitli kurtarma işlemleri yürütebileceksiniz."

#. Type: select
#. Description
#. :sl3:
#: ../rescue-mode.templates:3002
msgid ""
"If you choose not to use a root file system, you will be given a reduced "
"choice of operations that can be performed without one. This may be useful "
"if you need to correct a partitioning problem."
msgstr ""
"Bir kök dosya sistemi kullanmamayı seçerseniz kök dosya sistemi olmadan "
"yapabileceğiniz işlemlerin bir listesiyle karşılaşacaksınız. Bu, bölümleme "
"sorunlarını gidermek istediğinizde yararlı olabilir."

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:4001
msgid "No such device"
msgstr "Böyle bir aygıt yok"

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:4001
msgid ""
"The device you entered for your root file system (${DEVICE}) does not exist. "
"Please try again."
msgstr ""
"Kök dosya sistemi için girdiğiniz aygıt (${DEVICE}) mevcut değil. Lütfen "
"tekrar deneyin."

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:5001
msgid "Mount failed"
msgstr "Bağlama işlemi başarısız"

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:5001
msgid ""
"An error occurred while mounting the device you entered for your root file "
"system (${DEVICE}) on /target."
msgstr ""
"Kök dosya sistemi için girdiğiniz aygıt (${DEVICE}) /target'a bağlanırken "
"bir hata oluştu."

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:5001
msgid "Please check the syslog for more information."
msgstr "Lütfen daha fazla bilgi için syslog dosyasını inceleyin."

#. Type: select
#. Description
#. :sl3:
#: ../rescue-mode.templates:6001
msgid "Rescue operations"
msgstr "Kurtarma işlemleri"

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:7001
msgid "Rescue operation failed"
msgstr "Kurtarma işlemi başarısız"

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:7001
msgid "The rescue operation '${OPERATION}' failed with exit code ${CODE}."
msgstr ""
"Kurtarma işlemi '${OPERATION}', ${CODE} çıkış hata koduyla başarısız oldu."

#. Type: text
#. Description
#. :sl3:
#: ../rescue-mode.templates:8001
msgid "Execute a shell in ${DEVICE}"
msgstr "${DEVICE} aygıtında bir kabuk çalıştır"

#. Type: text
#. Description
#. :sl3:
#: ../rescue-mode.templates:9001
msgid "Execute a shell in the installer environment"
msgstr "Kurulum ortamında bir kabuk çalıştır"

#. Type: text
#. Description
#. :sl3:
#: ../rescue-mode.templates:10001
msgid "Choose a different root file system"
msgstr "Başka bir kök dosya sistemi seçin"

#. Type: text
#. Description
#. :sl3:
#: ../rescue-mode.templates:11001
msgid "Reboot the system"
msgstr "Sistemi yeniden başlat"

#. Type: text
#. Description
#. :sl3:
#. Type: text
#. Description
#. :sl3:
#. Type: text
#. Description
#. :sl3:
#: ../rescue-mode.templates:12001 ../rescue-mode.templates:16001
#: ../rescue-mode.templates:17001
msgid "Executing a shell"
msgstr "Bir kabuk çalıştırılıyor"

#. Type: text
#. Description
#. :sl3:
#: ../rescue-mode.templates:12001
msgid ""
"After this message, you will be given a shell with ${DEVICE} mounted on \"/"
"\". If you need any other file systems (such as a separate \"/usr\"), you "
"will have to mount those yourself."
msgstr ""
"Bu iletiden sonra, ${DEVICE} aygıtının \"/\" dosya sistemine bağlandığı bir "
"kabuğa gireceksiniz. Eğer başka dosya sistemlerine (ayrı bir \"/usr\" gibi) "
"ihtiyaç duyacaksanız bunları kendiniz bağlamalısınız."

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:13001
msgid "Error running shell in /target"
msgstr "Kabuk programı /target'da çalıştırılamadı"

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:13001
msgid ""
"A shell (${SHELL}) was found on your root file system (${DEVICE}), but an "
"error occurred while running it."
msgstr ""
"Kök dosya sisteminizde (${DEVICE}) bir kabuk (${SHELL}) bulundu, fakat kabuk "
"çalıştırılırken bir hata oluştu."

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:14001
msgid "No shell found in /target"
msgstr "/target'da herhangi bir kabuk bulunamadı"

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:14001
msgid "No usable shell was found on your root file system (${DEVICE})."
msgstr ""
"Kök dosya sisteminde (${DEVICE}) kullanılabilir durumda bir kabuk bulunamadı."

#. Type: text
#. Description
#. :sl3:
#: ../rescue-mode.templates:15001
msgid "Interactive shell on ${DEVICE}"
msgstr "${DEVICE} aygıtında etkileşimli kabuk"

#. Type: text
#. Description
#. :sl3:
#: ../rescue-mode.templates:16001
msgid ""
"After this message, you will be given a shell with ${DEVICE} mounted on \"/"
"target\". You may work on it using the tools available in the installer "
"environment. If you want to make it your root file system temporarily, run "
"\"chroot /target\". If you need any other file systems (such as a separate "
"\"/usr\"), you will have to mount those yourself."
msgstr ""
"Bu iletiden sonra ${DEVICE} aygıtının \"/target\" dosya sistemine bağlandığı "
"bir kabuğa gireceksiniz. Kurulum ortamında mevcut araçları kullanarak bu "
"dosya sisteminde çalışabilirsiniz. Geçici olarak bunu kök dosya sistemi "
"yapmak istiyorsanız \"chroot /target\" komutunu çalıştırın. Eğer başka dosya "
"sistemlerine (ayrı bir \"/usr\" gibi) ihtiyaç duyacaksanız bunları kendiniz "
"bağlamalısınız."

#. Type: text
#. Description
#. :sl3:
#: ../rescue-mode.templates:17001
msgid ""
"After this message, you will be given a shell in the installer environment. "
"No file systems have been mounted."
msgstr ""
"Bu iletiden sonra, kurulum ortamındaki bir kabuğa gireceksiniz. Bu kabukta "
"herhangi bir dosya sistemi bağlı olmayacak."

#. Type: text
#. Description
#. :sl3:
#: ../rescue-mode.templates:18001
msgid "Interactive shell in the installer environment"
msgstr "Kurulum ortamında etkileşimli kabuk"

#. Type: password
#. Description
#. :sl3:
#: ../rescue-mode.templates:19001
msgid "Passphrase for ${DEVICE}:"
msgstr "${DEVICE} için parola:"

#. Type: password
#. Description
#. :sl3:
#: ../rescue-mode.templates:19001
msgid "Please enter the passphrase for the encrypted volume ${DEVICE}."
msgstr "Şifrelenmiş ${DEVICE} cildi için bir geçiş parolası gerekiyor."

#. Type: password
#. Description
#. :sl3:
#: ../rescue-mode.templates:19001
msgid ""
"If you don't enter anything, the volume will not be available during rescue "
"operations."
msgstr ""
"Herhangi bir şey girmezseniz, kurtarma işlemi süresince bu cilde "
"erişemeyeceksiniz."

#. Type: multiselect
#. Choices
#. :sl3:
#: ../rescue-mode.templates:20001
msgid "Automatic"
msgstr "Otomatik"

#. Type: multiselect
#. Description
#. :sl3:
#: ../rescue-mode.templates:20002
msgid "Partitions to assemble:"
msgstr "Birleştirilecek bölümler:"

#. Type: multiselect
#. Description
#. :sl3:
#: ../rescue-mode.templates:20002
msgid ""
"Select the partitions to assemble into a RAID array. If you select "
"\"Automatic\", then all devices containing RAID physical volumes will be "
"scanned and assembled."
msgstr ""
"Bir RAID dizisine birleştirilecek olan bölümleri seçin. Eğer \"Otomatik\" "
"seçeneğini seçerseniz, RAID fiziksel ciltleri içeren tüm aygıtlar taranacak "
"ve birleştirilecek."

#. Type: multiselect
#. Description
#. :sl3:
#: ../rescue-mode.templates:20002
msgid ""
"Note that a RAID partition at the end of a disk may sometimes cause that "
"disk to be mistakenly detected as containing a RAID physical volume. In that "
"case, you should select the appropriate partitions individually."
msgstr ""
"Diskin sonundaki bir RAID bölümünün yanlışlıkla, diskin RAID fiziksel cildi "
"içeriyormuş gibi algılanmasına neden olabileceğini göz önünde bulundurun. "
"Böyle bir durumda uygun bölümleri tek tek seçmeniz gerekir."

#. Type: boolean
#. Description
#. :sl3:
#: ../rescue-mode.templates:21001
msgid "Mount separate ${FILESYSTEM} partition?"
msgstr "Ayrı bir ${FILESYSTEM} bölümü bağlansın mı?"

#. Type: boolean
#. Description
#. :sl3:
#: ../rescue-mode.templates:21001
msgid "The installed system appears to use a separate ${FILESYSTEM} partition."
msgstr "Kurulan sistem ayrı bir ${FILESYSTEM} bölümü kullanıyor."

#. Type: boolean
#. Description
#. :sl3:
#: ../rescue-mode.templates:21001
msgid ""
"It is normally a good idea to mount it as it will allow operations such as "
"reinstalling the boot loader. However, if the file system on ${FILESYSTEM} "
"is corrupt then you may want to avoid mounting it."
msgstr ""
"Açılış önyükleyicisini tekrar kurmak gibi işlemlere izin verdiği için bu "
"bölümü bağlamak normalde iyi bir fikirdir. Ancak ${FILESYSTEM} dosya sistemi "
"hasarlıysa bağlamak istemeyebilirsiniz."
