# THIS FILE IS GENERATED AUTOMATICALLY FROM THE D-I PO MASTER FILES
# The master files can be found under packages/po/
#
# DO NOT MODIFY THIS FILE DIRECTLY: SUCH CHANGES WILL BE LOST
#
# Simplified Chinese translation for Debian Installer.
#
# Copyright (C) 2003-2008 Software in the Public Interest, Inc.
# This file is distributed under the same license as debian-installer.
#
# Translated by Yijun Yuan (2004), Carlos Z.F. Liu (2004,2005,2006),
# Ming Hua (2005,2006,2007,2008), Xiyue Deng (2008), Kov Chai (2008),
# Kenlen Lai (2008), WCM (2008), Ren Xiaolei (2008).
#
#
# Translations from iso-codes:
#   Tobias Toedter <t.toedter@gmx.net>, 2007.
#     Translations taken from ICU SVN on 2007-09-09
#
#   Free Software Foundation, Inc., 2002, 2003, 2007, 2008.
#   Alastair McKinstry <mckinstry@computer.org>, 2001,2002.
#   Translations taken from KDE:
#   - Wang Jian <lark@linux.net.cn>, 2000.
#   - Carlos Z.F. Liu <carlosliu@users.sourceforge.net>, 2004 - 2006.
#   LI Daobing <lidaobing@gmail.com>, 2007, 2008, 2009, 2010.
#   YunQiang Su <wzssyqa@gmail.com>, 2011.
#
#   Mai Hao Hui <mhh@126.com>, 2001 (translations from galeon)
# YunQiang Su <wzssyqa@gmail.com>, 2010, 2011, 2012, 2013.
# Yangfl <mmyangfl@gmail.com>, 2017.
# Boyuan Yang <073plan@gmail.com>, 2018, 2019, 2020.
# 玉堂白鹤 <yjwork@qq.com>, 2020.
# TTT DEAL <ikmn2jlt@linshiyouxiang.net>, 2021.
# Wenbin Lv <wenbin816@gmail.com>, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: debian-installer\n"
"Report-Msgid-Bugs-To: rescue@packages.debian.org\n"
"POT-Creation-Date: 2019-11-02 21:37+0100\n"
"PO-Revision-Date: 2022-04-17 17:12+0000\n"
"Last-Translator: Wenbin Lv <wenbin816@gmail.com>\n"
"Language-Team: <debian-l10n-chinese@lists.debian.org>\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#. Type: title
#. Description
#. Info message displayed when running in rescue mode
#. :sl2:
#: ../rescue-check.templates:2001
msgid "Rescue mode"
msgstr "急救模式"

#. Type: text
#. Description
#. :sl1:
#: ../rescue-mode.templates:1001
msgid "Enter rescue mode"
msgstr "进入急救模式"

#. Type: text
#. Description
#. :sl3:
#: ../rescue-mode.templates:2001
msgid "No partitions found"
msgstr "未找到分区"

#. Type: text
#. Description
#. :sl3:
#: ../rescue-mode.templates:2001
msgid ""
"The installer could not find any partitions, so you will not be able to "
"mount a root file system. This may be caused by the kernel failing to detect "
"your hard disk drive or failing to read the partition table, or the disk may "
"be unpartitioned. If you wish, you may investigate this from a shell in the "
"installer environment."
msgstr ""
"安装程序无法找到任何分区，所以您将无法挂载根文件系统。这可能是由内核检测您的"
"硬盘驱动器失败，或者读取分区表失败，或者磁盘还未分区所导致。如果您愿意，您可"
"以在安装程序环境中的 shell 里对此进行调查。"

#. Type: select
#. Choices
#. :sl3:
#: ../rescue-mode.templates:3001
msgid "Assemble RAID array"
msgstr "装配 RAID 阵列"

#. Type: select
#. Choices
#. :sl3:
#: ../rescue-mode.templates:3001
msgid "Do not use a root file system"
msgstr "不使用根文件系统"

#. Type: select
#. Description
#. :sl3:
#: ../rescue-mode.templates:3002
msgid "Device to use as root file system:"
msgstr "要用作根文件系统的设备："

#. Type: select
#. Description
#. :sl3:
#: ../rescue-mode.templates:3002
msgid ""
"Enter a device you wish to use as your root file system. You will be able to "
"choose among various rescue operations to perform on this file system."
msgstr ""
"请输入一个您想要用作根文件系统的设备。您将可以选择各种能在这个文件系统上实行"
"的急救措施。"

#. Type: select
#. Description
#. :sl3:
#: ../rescue-mode.templates:3002
msgid ""
"If you choose not to use a root file system, you will be given a reduced "
"choice of operations that can be performed without one. This may be useful "
"if you need to correct a partitioning problem."
msgstr ""
"如果您选择不使用根文件系统，您将会看到一个不需要根文件系统即可进行的操作列"
"表，可选择的操作会少一些。如果您需要修复分区问题，选择该选项可能会有用。"

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:4001
msgid "No such device"
msgstr "此设备不存在"

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:4001
msgid ""
"The device you entered for your root file system (${DEVICE}) does not exist. "
"Please try again."
msgstr "您所输入的作为根文件系统的设备 (${DEVICE}) 并不存在。请重试。"

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:5001
msgid "Mount failed"
msgstr "挂载失败"

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:5001
msgid ""
"An error occurred while mounting the device you entered for your root file "
"system (${DEVICE}) on /target."
msgstr "挂载您所输入的作为根文件系统的设备 (${DEVICE}) 到 /target 时出现错误。"

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:5001
msgid "Please check the syslog for more information."
msgstr "请检查系统日志 (syslog) 来获取详细信息。"

#. Type: select
#. Description
#. :sl3:
#: ../rescue-mode.templates:6001
msgid "Rescue operations"
msgstr "急救操作"

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:7001
msgid "Rescue operation failed"
msgstr "急救操作失败"

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:7001
msgid "The rescue operation '${OPERATION}' failed with exit code ${CODE}."
msgstr "急救操作 \"${OPERATION}\" 失败，退出代码为 ${CODE}。"

#. Type: text
#. Description
#. :sl3:
#: ../rescue-mode.templates:8001
msgid "Execute a shell in ${DEVICE}"
msgstr "在 ${DEVICE} 中运行 shell"

#. Type: text
#. Description
#. :sl3:
#: ../rescue-mode.templates:9001
msgid "Execute a shell in the installer environment"
msgstr "在安装程序环境中运行 shell"

#. Type: text
#. Description
#. :sl3:
#: ../rescue-mode.templates:10001
msgid "Choose a different root file system"
msgstr "选择另一个根文件系统"

#. Type: text
#. Description
#. :sl3:
#: ../rescue-mode.templates:11001
msgid "Reboot the system"
msgstr "重新启动系统"

#. Type: text
#. Description
#. :sl3:
#. Type: text
#. Description
#. :sl3:
#. Type: text
#. Description
#. :sl3:
#: ../rescue-mode.templates:12001 ../rescue-mode.templates:16001
#: ../rescue-mode.templates:17001
msgid "Executing a shell"
msgstr "正在运行 shell"

#. Type: text
#. Description
#. :sl3:
#: ../rescue-mode.templates:12001
msgid ""
"After this message, you will be given a shell with ${DEVICE} mounted on \"/"
"\". If you need any other file systems (such as a separate \"/usr\"), you "
"will have to mount those yourself."
msgstr ""
"在这条信息之后，您将得到一个 shell，其中 ${DEVICE} 已被挂载到 \"/\"。如果您还"
"需要其他文件系统 (例如一个单独的 \"/usr\")，您必须自行挂载它们。"

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:13001
msgid "Error running shell in /target"
msgstr "在 /target 中运行 shell 时出错"

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:13001
msgid ""
"A shell (${SHELL}) was found on your root file system (${DEVICE}), but an "
"error occurred while running it."
msgstr ""
"在您的根文件系统 (${DEVICE}) 上找到了一个 shell (${SHELL})，但在运行它时出现"
"了错误。"

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:14001
msgid "No shell found in /target"
msgstr "未能在 /target 上找到 shell"

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:14001
msgid "No usable shell was found on your root file system (${DEVICE})."
msgstr "未能在您的根文件系统 (${DEVICE}) 上找到可用的 shell。"

#. Type: text
#. Description
#. :sl3:
#: ../rescue-mode.templates:15001
msgid "Interactive shell on ${DEVICE}"
msgstr "在 ${DEVICE} 中的交互式 shell"

#. Type: text
#. Description
#. :sl3:
#: ../rescue-mode.templates:16001
msgid ""
"After this message, you will be given a shell with ${DEVICE} mounted on \"/"
"target\". You may work on it using the tools available in the installer "
"environment. If you want to make it your root file system temporarily, run "
"\"chroot /target\". If you need any other file systems (such as a separate "
"\"/usr\"), you will have to mount those yourself."
msgstr ""
"在这条信息之后，您将得到一个 shell，其中 ${DEVICE} 已被挂载到 \"/target\"。您"
"可以利用安装程序环境中提供的工具对其进行操作。如果您想暂时使其成为您的根文件"
"系统，请运行 \"chroot /target\"。如果您还需要其他文件系统 (例如一个单独的 \"/"
"usr\")，您必须自行挂载它们。"

#. Type: text
#. Description
#. :sl3:
#: ../rescue-mode.templates:17001
msgid ""
"After this message, you will be given a shell in the installer environment. "
"No file systems have been mounted."
msgstr ""
"在这条信息之后，您将得到一个安装程序环境中的 shell。没有挂载任何文件系统。"

#. Type: text
#. Description
#. :sl3:
#: ../rescue-mode.templates:18001
msgid "Interactive shell in the installer environment"
msgstr "安装程序环境中的交互式 shell"

#. Type: password
#. Description
#. :sl3:
#: ../rescue-mode.templates:19001
msgid "Passphrase for ${DEVICE}:"
msgstr "${DEVICE} 的密码："

#. Type: password
#. Description
#. :sl3:
#: ../rescue-mode.templates:19001
msgid "Please enter the passphrase for the encrypted volume ${DEVICE}."
msgstr "请输入加密卷 ${DEVICE} 的密码。"

#. Type: password
#. Description
#. :sl3:
#: ../rescue-mode.templates:19001
msgid ""
"If you don't enter anything, the volume will not be available during rescue "
"operations."
msgstr "如果您什么都不输入，急救操作时将无法使用这一磁盘卷。"

#. Type: multiselect
#. Choices
#. :sl3:
#: ../rescue-mode.templates:20001
msgid "Automatic"
msgstr "自动"

#. Type: multiselect
#. Description
#. :sl3:
#: ../rescue-mode.templates:20002
msgid "Partitions to assemble:"
msgstr "要装配的分区："

#. Type: multiselect
#. Description
#. :sl3:
#: ../rescue-mode.templates:20002
msgid ""
"Select the partitions to assemble into a RAID array. If you select "
"\"Automatic\", then all devices containing RAID physical volumes will be "
"scanned and assembled."
msgstr ""
"选择一个分区以装配到一个 RAID 阵列。如果选择 \"自动\"，将扫描并装配所有包含 "
"RAID 物理卷的设备。"

#. Type: multiselect
#. Description
#. :sl3:
#: ../rescue-mode.templates:20002
msgid ""
"Note that a RAID partition at the end of a disk may sometimes cause that "
"disk to be mistakenly detected as containing a RAID physical volume. In that "
"case, you should select the appropriate partitions individually."
msgstr ""
"注意：磁盘尾部的 RAID 分区有时会导致磁盘被错误地识别为包含 RAID 物理卷的磁"
"盘。这种情况下，您需要单独选择合适的分区。"

#. Type: boolean
#. Description
#. :sl3:
#: ../rescue-mode.templates:21001
msgid "Mount separate ${FILESYSTEM} partition?"
msgstr "挂载单独的 ${FILESYSTEM} 分区？"

#. Type: boolean
#. Description
#. :sl3:
#: ../rescue-mode.templates:21001
msgid "The installed system appears to use a separate ${FILESYSTEM} partition."
msgstr "安装的系统要使用单独的 ${FILESYSTEM} 分区。"

#. Type: boolean
#. Description
#. :sl3:
#: ../rescue-mode.templates:21001
msgid ""
"It is normally a good idea to mount it as it will allow operations such as "
"reinstalling the boot loader. However, if the file system on ${FILESYSTEM} "
"is corrupt then you may want to avoid mounting it."
msgstr ""
"通常将其挂载是个好主意，因为这将允许您执行重新安装启动引导器等操作。但是如果 "
"${FILESYSTEM} 上的文件系统已损坏，则您可能不希望挂载它。"
